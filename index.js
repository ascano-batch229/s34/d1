// Default setup of Express
// "require" to load the module/package

const express = require("express");

//  Creation of an app/project
// In layman's terms, app is our server
const app = express();
const port = 3000;

// Setup for allowing the server to handle data request
// Middlewares 

app.use(express.json()); // allowing system to handle API
// allows your app to read data from forms
app.use(express.urlencoded({extended:true})); //reads inputs from users

// [SECTION] - Routes

// GET Method
app.get("/", (req,res) => {
	res.send("Hello World!");
});

/*app.get("/hello", (req,res) => {
	res.send(`Hello There ${req.body.firstName} ${req.body.lastName}!`);
});*/


// POST Method
app.post("/hello", (req,res) => {
	res.send(`Hello There ${req.body.firstName} ${req.body.lastName}!`);
});

// MOCK DATABASE
// An array that will store user object when the "/signup" route is accessed
// This will serve as our mock database

let users = [];

//POST METHOD - Signup
app.post("/signup", (req,res) => {

	console.log(req.body);

	// if the username and password is not empty the user info will be pushed/store to our mock database.
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	}else{
		res.send("Please input BOTH username and password");
	}
});

// PUT/UPDATE METHOD

app.put("/change-password", (req,res) => {
	// Creates a variable to store the message to be sent back to the client/users.
	let message;

	// Creates a for loop that will loop through the elements of "users" array.
	for(let i = 0; i < users.length; i++){

		// If the username provided in the client/postman and the username of the current object is the same.
		if(req.body.username == users[i].username){

			// Changes the password of the users found by the loop
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated.`;

			break;
		}else{
			// If no user found
			message = "User does not exist.";
		}
		
	}

	res.send(message);

});

// CODE ALONG ACTIVITY 

// Homes page

app.get("/home", (req,res) => {

	res.send("Welcome to Homepage");
});

// REGISTERED Users
app.get("/users", (req,res) => {

	res.send(users);
});

// DELETE USER

app.delete("/delete-user", (req,res) => {

	let message;

	if(users.length != 0){
		for(i = 0; i < users.length; i++){
			if(req.body.username == users[i].username){
				users.splice(users[i].username, 1);
				
				message = `User ${req.body.username} has been deleted`;

				break;
			}else{

				 message = "User does not exist";
			}

		}

	}else{

		message = "User is empty";
	}
	res.send(message);


})



app.listen(port, () => console.log(`Server is running at port ${port}.`));